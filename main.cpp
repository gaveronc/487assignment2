/*
* Cameron Gaveronski
* 200230075
* ENEL 487
* Assignment 2
* Due October 13, 2016
*/

#include <iostream>  //Used for I/O
#include <sstream>   //Used to convert strings to doubles
#include <cmath>     //Used for sin/cos functions
#include "Complex.h" //Complex data type definition

using namespace std;

//Converts a string to a double
double StringToD (string input);

//This program assumes the input is correct, meaning the following order:
//<operation> <real component 1> <imaginary component 1> <real component 2> <imaginary component 2>
//This convention is followed in both interactive mode and batch mode
//For assignment 2, this program has been expanded to also perform unary operations in the following form:
//<operation> <real component> <imaginary component>
int main(int argc, char *argv[]) {
	Complex a, b, result;
	
	//This determines which of the above functions are called, as well as exiting on a 'q'
	string inputLine = "";
	string operation = "";
	
	if (argc == 1) {
		//Interactive mode
	} else {
		if (argc == 4) {
			//Single-number operation
			operation = argv[1];
			a.SetReal(StringToD(argv[2]));
			a.SetImaginary(StringToD(argv[3]));
			//Make first argument lower case
			for(unsigned i = 0; i < operation.length(); i++) {
				if(operation[i] < 'a') {
					operation[i] += 32;
				}
			}
			
			PerformOperation(a, operation);
		} else {
			if (argc == 6) {
				//Batch mode, used as a function call
				//Follows convention specified above
				operation = argv[1];
				//Make first argument lower case
				for(unsigned i = 0; i < operation.length(); i++) {
					if(operation[i] < 'a') {
						operation[i] += 32;
					}
				}
				
				a.SetReal(StringToD(argv[2]));
				a.SetImaginary(StringToD(argv[3]));
				b.SetReal(StringToD(argv[4]));
				b.SetImaginary(StringToD(argv[5]));
				
				cerr << a.GetReal() << endl;
				cerr << a.GetImaginary() << endl;
				cerr << b.GetReal() << endl;
				cerr << b.GetImaginary() << endl;
				
				//Choose which operation to perform on the data
				result = PerformOperation(a, b, operation);
				
				//Format output
				cout << result.GetReal();
				if (result.GetImaginary() < 0) {
					cout << " - j " << -result.GetImaginary();
				} else {
					cout << " + j " << result.GetImaginary();
				}
				cout << endl;
			} else {
				cerr << "Bad arguments\n";
			}
		}
		return 0;
	}
	
	//Interactive mode loop
	//Exits if input is 'q'
	while (inputLine != "q" && inputLine != "Q") {
		string temp;
		cerr << "Enter an operation: ";
		getline(cin, inputLine);
		if (inputLine != "q" && inputLine != "Q") {
			//Remove leading and trailing whitespaces
			int first = inputLine.find_first_not_of(' ');
			int last = inputLine.find_last_not_of(' ');
			inputLine = inputLine.substr(first, (last-first+1));
			
			//Get arguments and argument count
			int arglength;
			string args[5];
			for (arglength = 0; inputLine.find(" ") != string::npos; arglength++) {
				args[arglength] = inputLine.substr(0, inputLine.find(" "));
				first = inputLine.find(" ");
				inputLine = inputLine.substr(first + 1, inputLine.length() - first);
				first = inputLine.find_first_not_of(' ');
				inputLine = inputLine.substr(first, inputLine.length() - first + 1);
			}
			args[arglength] = inputLine;//Last argument
			
			//Make first argument lower case
			for(unsigned i = 0; i < args[0].length(); i++) {
				if(args[0][i] < 'a') {
					args[0][i] += 32;
				}
			}
			
			if (arglength == 4) {
				//Perform binary operation
				a.SetReal(StringToD(args[1]));
				a.SetImaginary(StringToD(args[2]));
				b.SetReal(StringToD(args[3]));
				b.SetImaginary(StringToD(args[4]));
				
				result = PerformOperation(a, b, args[0]);
				//Format output
				cout << result.GetReal();
				if (result.GetImaginary() < 0) {
					cout << " - j " << -result.GetImaginary();
				} else {
					cout << " + j " << result.GetImaginary();
				}
				cout << endl;
			} else {
				if (arglength == 2) {
					//Perform unary operation
					a.SetReal(StringToD(args[1]));
					a.SetImaginary(StringToD(args[2]));
					
					//This function also prints the output
					PerformOperation(a, args[0]);
				} else {
					//Invalid input
					cerr << "Bad arguments\n";
				}
			}
		}
	}
	
	return 0;
}

//Converts a string to a double
double StringToD (string input) {
	stringstream stream;
	double strDouble;
	
	//Ensure the stream is empty
	stream.clear();
	//Put the string into the stream
	stream << input;
	//Get a double out of the stream
	stream >> strDouble;
	
	return strDouble;
}

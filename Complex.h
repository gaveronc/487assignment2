#ifndef COMPLEX__H
#define COMPLEX__H

#define NUM_OPS_1 5
#define NUM_OPS_2 4

#include <string>
#include <cmath> //Used for atan2 function

class Complex {
	public:
		void SetReal(double x);
		void SetImaginary(double x);
		
		//Rectangular values
		double GetReal();
		double GetImaginary();
		
		//Polar values, calculated as needed
		double GetMagnitude();
		double GetAngle();
	private: //Only stores rectangular coordinates
		double real;
		double imaginary;
};

//Map strings to function calls for unary operations
const std::string OP_STRINGS_1[NUM_OPS_1] = {
	"abs",
	"arg",
	"argdeg",
	"exp",
	"inv"
};

//Map strings to function calls for binary operations
const std::string OP_STRINGS_2[NUM_OPS_2] = {
	"a",
	"s",
	"m",
	"d"
};

//Binary functions

//Adds complex number b to a and stores the result in the last argument
void Add(Complex a, Complex b, Complex *result);

//Subtracts complex number b from a and stores the result in the last argument
void Subtract(Complex a, Complex b, Complex *result);

//Multiplies complex number b and a and stores the result in the last argument
void Multiply(Complex a, Complex b, Complex *result);

//Divides complex number a by b and stores the result in the last argument
void Divide(Complex a, Complex b, Complex *result);


//Unary functions

//Get magnitude of number
void absolute (Complex a);

//Get angle in radians
void argrad (Complex a);

//Get angle in degrees
void argdeg (Complex a);

//Get exponent
void exponential (Complex a);

//Get inverse
void inverse (Complex a);

static void (*FUNCTION_CALLS_1[NUM_OPS_1])(Complex a) = {
	absolute,
	argrad,
	argdeg,
	exponential,
	inverse
};

static void (*FUNCTION_CALLS_2[NUM_OPS_2])(Complex a, Complex b, Complex *result) = {
	Add,
	Subtract,
	Multiply,
	Divide
};

//Operation functions
Complex PerformOperation(Complex a, Complex b, std::string operation);
void PerformOperation(Complex a, std::string operation);

#endif

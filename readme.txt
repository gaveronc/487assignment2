This program performs mathematical operations on complex numbers, taking input
in the form "operation real_1 imaginary_1 real_2 imaginary_2", where
"operation" is one of the characters "a" "s" "m" "d" or "q", which correspond to
addition, subtraction, multiplication, division, and stopping the program,
respectively.

This assignment has expanded the program to include some unary operations, like
finding the inverse or getting the magnitude of a vector.

The program has two modes of operation, which depend on the number of
arguments used to invoke the executable. If no arguments are present, then the
program is launched in interactive mode where the user will be continually
prompted for operations and data until the operation "q" is provided.

The program can also be executed with three or five arguments which will
perform a single operation and then exit, using the same argument order as the
interactive mode. Three arguments are used for a unary operation, and five are
used for a binary operation.

The program can be compiled by invoking the makefile using make. The output
file will be call "A2", and can be invoked from the command line like any
program.
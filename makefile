# the compiler: gcc for C program, define as g++ for C++
CC = g++

# compiler flags:
#  -g    adds debugging information to the executable file
#  -Wall turns on most, but not all, compiler warnings
CFLAGS  = -g -Wall

# the build target executable:
FILESCPP = Complex.cpp main.cpp
TARGET = A2

all:
	$(CC) $(CFLAGS) -o $(TARGET) $(FILESCPP)

clean:
	$(RM) $(TARGET)

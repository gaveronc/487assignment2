#include "Complex.h"
#include <math.h>
#include <iostream>

using namespace std;

void Complex::SetReal(double x) {
	real = x;
}

void Complex::SetImaginary(double x) {
	imaginary = x;
}

double Complex::GetReal() {
	return real;
}

double Complex::GetImaginary() {
	return imaginary;
}

//Magnitude is the square root of the sum of the squares
double Complex::GetMagnitude() {
	return sqrt((real*real)+(imaginary*imaginary));
}

//Angle is tan^-1(imaginary/real)
double Complex::GetAngle() {
	return atan2(imaginary,real);
}

//Adds complex number b to a and stores the result in the last argument
void Add(Complex a, Complex b, Complex *result) {
	//Add b components to a components
	result->SetReal(a.GetReal() + b.GetReal());
	result->SetImaginary(a.GetImaginary() + b.GetImaginary());
}

//Subtracts complex number b from a and stores the result in the last argument
void Subtract(Complex a, Complex b, Complex *result) {
	//Subtracy b components from a components
	result->SetReal(a.GetReal() - b.GetReal());
	result->SetImaginary(a.GetImaginary() - b.GetImaginary());
}

//Multiplies complex number b and a and stores the result in the last argument
void Multiply(Complex a, Complex b, Complex *result) {
	double magnitude;
	double angle;
	
	//Multiply magnitudes
	magnitude = a.GetMagnitude()*b.GetMagnitude();
	//Add angles
	angle = a.GetAngle()+b.GetAngle();
	//Convert to rectangular form
	result->SetReal(cos(angle)*magnitude);
	result->SetImaginary(sin(angle)*magnitude);
}

//Get magnitude of number
void absolute (Complex a) {
	cout << a.GetMagnitude() << endl;
}

//Get angle in radians
void argrad (Complex a) {
	cout << a.GetAngle() << endl;
}

//Get angle in degrees
void argdeg (Complex a) {
	cout << a.GetAngle()*360/(2*M_PI) << endl;
}

//Get exponent e^a
//e^(x+jy) = e^x(cos(y) + i*sin(y))
void exponential (Complex a) {
	double mul, real, imaginary;
	mul = exp(a.GetReal());
	real = cos(a.GetImaginary()) * mul;
	imaginary = sin(a.GetImaginary()) * mul;
	if (imaginary > 0) {
		cout << real << " + j " << imaginary << endl;
	} else {
		cout << real << " - j " << -imaginary << endl;
	}
}

//Get inverse
void inverse (Complex a) {
	double mag, angle, real, imaginary;
	mag = 1/a.GetMagnitude();
	angle = -a.GetAngle();
	real = cos(angle)*mag;
	imaginary = sin(angle)*mag;
	if (imaginary > 0) {
		cout << real << " + j " << imaginary << endl;
	} else {
		cout << real << " - j " << -imaginary << endl;
	}
}

//Divides complex number a by b and stores the result in the last argument
void Divide(Complex a, Complex b, Complex *result) {
	double magnitude;
	double angle;
	
	//Divide magnitudes
	magnitude = a.GetMagnitude()/b.GetMagnitude();
	//Subtract angles
	angle = a.GetAngle()-b.GetAngle();
	//Convert to rectangular form
	result->SetReal(cos(angle)*magnitude);
	result->SetImaginary(sin(angle)*magnitude);
}

//Overloaded function, this one performs binary operations
Complex PerformOperation(Complex a, Complex b, std::string operation) {
	Complex result;
	for (int i = 0; i < NUM_OPS_2; i++) {
		if (operation == OP_STRINGS_2[i]) {
			FUNCTION_CALLS_2[i](a, b, &result);
			break;
		}
	}
	return result;
}

//Overloaded function, this one performs unary operations
void PerformOperation(Complex a, std::string operation) {
	for (int i = 0; i < NUM_OPS_1; i++) {
		if (operation == OP_STRINGS_1[i]) {
			FUNCTION_CALLS_1[i](a);
			break;
		}
	}
}
